---
layout: page
title: Developer guide
permalink: /devguide/
---

This is the developer's guide.  It should include:

- architecture of DEEPSEA
- design decisions
- limitations

## Support

- Slack
- IRC channel: `#deepseateam` on `irc.freenode.ent`

## Contributing to DEEPSEA

- [How to contribute to the project](/devguide/contribute)
- [The code of conduct](/devguide/codeofconduct)
- [Our style guide](/devguide/styleguide)
- [Write good commit messages](/devguide/commitmsgs)
- [List of contributors](/devguide/contributors)

