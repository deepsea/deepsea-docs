---
layout:     post
title:      First DEEPSEA video released
author:     Jaco Geldenhuys
categories: news
---

We have just released the first DEEPSEA video.  It explains how to install Z3,
which a critical library that DEEPSEA uses to generate new inputs values.
(Although it is possible to run DEEPSEA without Z3, it is not easy, and this
installing Z3 is highly recommended.)  The video covers OS X, Linux, and
Windows.

